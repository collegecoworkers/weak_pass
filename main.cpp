#include <fstream>
#include <iostream>
#include <string>

using namespace std;

bool is_weak_password(string pass){

	ifstream infile("pass.txt");

	char buf[30];

	while(!infile.eof()){
		// reding one string in variable 'buf', 30 - max length of string
		infile.getline(buf, 30);
		if(pass == buf)
			return true;
	}
	infile.close();

	return false;
}

int main(int argc, char const **argv){
	string input;

	while(true){
		cout << "\n\nEnter 'exit' for exit \n";
		cout << "Enter your password: ";
		getline(cin, input);

		if(input == "exit") break;

		if(is_weak_password(input)){
			cout << "Your password is weak";
		} else{
			cout << "Your password is not weak";
		}
	}

	return 0;
}